// *************************************************************************
//  Agglomerative Mean-Shift Clustering
//
//  Reference: Xiao-Tong Yuan, Bao-Gang Hu and Ran He, Agglomerative Mean-Shift Clustering, IEEE Transactions on Knowledge and Data Engineering
//
//   Written by Xiao-Tong Yuan, 2009-2010.
// *************************************************************************

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA


#include <mex.h>

typedef struct 
{
   double* center;
   double radius;
   double weight;
} sphere;


//interface for FastGauss:
class Agglo_MS{
public:
//------------------------------------------------------------------------------------//
	//	data structure
	// interface:
	
    int data_dim;	// data dimension.
	int ref_data_N;	// current reference data length.
	int query_data_N;
    int data_N_original; // input reference data length
	int center_N;
	double bandwid;		//bandwidth
	int iteration_num;

	// internal data:
    double* Input_Data;
	double* Ref_Data;
	double* Ref_Weight;
	double* Query_Data;
	double* Query_Weight;
    double* mode_total;
    double* mode_ind;
    
	// for set covering
	sphere* sph_set;
	int sph_count;
	double* sph_mode;
    double* sph_count_total; 
//---------------------------------------------------------------------------------------//	
	//	public functions
public:
	
	//constructors
	Agglo_MS(double* Data, int dim, int N, double* ref_data, int ref_data_n, double* ref_weight,double band, int iten);
	//destructor
	~Agglo_MS();	

	void sphere_construction(double* x_init, double weight);

	void set_compression();

};