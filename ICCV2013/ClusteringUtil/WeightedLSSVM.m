function [model, decision_val] = WeightedLSSVM(x, y, Ws, C)
K = x*x';
n = size(x, 1);
Vr = diag(1./(C*Ws));

B = K + Vr;
A = zeros(n+1);
A(1,2:end) = 1;
A(2:end, 1) = 1;
A(2:end, 2:end) = B;
Tmp = inv(A)*[0; y];

model.w = sum(diag(Tmp(2:end))*x,1);
model.b = Tmp(1);
decision_val = model.w*x' + model.b;
end