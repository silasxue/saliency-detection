function [map] = sparse_saliency_v2(featureMap, blockSize, alpha, lambda, Dict, MethodName)
%%
%init
img = double(featureMap);
[m n c] = size(img);

%parameter for the Lasso
param.lambda = lambda;
param.mode = 2;

%pre allocated memory
map = zeros([m n]);
center = zeros([blockSize*blockSize*c 1]);
tempPatch = zeros([blockSize blockSize c]);
stepLength = fix(blockSize./2);                %half overlapping
csLength = fix(blockSize./2);
total = round((m-blockSize)./stepLength)*round((n-blockSize)./stepLength);

progress = 0;                                              
tic

for i=1:stepLength:m-blockSize+1
    for j=1:stepLength:n-blockSize+1       
        %clear the memory
        A = zeros([blockSize*blockSize*c 1]);
        
        %location of the center patch
        ctop = i;
        cbottom = i+blockSize-1;  
        cleft = j;
        cright = j+blockSize-1;
               
        %location of the surroundings patch
        stop = max([1 i-fix(alpha./2)*blockSize]);
        sbottom = min([m i+blockSize-1+fix(alpha./2)*blockSize]); 
        sleft = max([1 j-fix(alpha./2)*blockSize]);
        sright = min([n j+blockSize-1+fix(alpha./2)*blockSize]);
        sm = sbottom-stop+1;
        sn = sright-sleft+1;
        
        %location of the center in the surrounding patch
        top = ctop-stop+1;
        bottom = top+blockSize-1;
        left = cleft-sleft+1;
        right = left+blockSize-1;
        
        %vector of the center
        tempPatch = img(ctop:cbottom,cleft:cright,:);
        center = tempPatch(1:blockSize*blockSize*c)';
               
        %get all the surrounding patches to form overcomplete dictionary A
        count=1;
        for k=1:csLength:sm-blockSize+1
            for l=1:csLength:sn-blockSize+1
                if k+blockSize-1<top || k>bottom || l+blockSize-1<left || l>right
                    tempPatch = img(stop+k-1:stop+k-2+blockSize,sleft+l-1:sleft+l-2+blockSize,:);
                    A(:,count) = tempPatch(1:blockSize*blockSize*c)';
                    count = count+1;
                end
            end
        end
        
        if ~isempty(Dict)          
            center= mexLasso(center, Dict,param);
            A= mexLasso(A, Dict, param);
        end
              
        %incremental sparse coding
        switch MethodName
            case 'sc'
                x = mexLasso(center, A, param);
                map(ctop:cbottom,cleft:cright) = map(ctop:cbottom,cleft:cright)+ sum(abs(x));
            case 'svmc'              
                %margin
                SurSampSize = size(A, 2);
                SamplingNum = 1; SaArr = zeros(1,SamplingNum);
                for kk=1:SamplingNum
                    TotalIdxSet = [1:SurSampSize];
                    SampIdx = randsample(SurSampSize, floor(SurSampSize*0.3));
                    SampA = A(:, SampIdx); TotalIdxSet(SampIdx) = [];
                    train_label = -ones(1+size(SampA,2),1); train_label(1) = 1;
                    train_inst = [center SampA]';
                    model = libsvmtrain(train_label, train_inst, '-s 0 -t 0 -c 1 -w1 5 -w-1 0.01');
                    [predict_label, accuracy, dec_values] = libsvmpredict(ones(length(TotalIdxSet), 1), A(:,TotalIdxSet)', model);
                    negidx = find(predict_label<0);
                    %dec_values = exp(dec_values); dec_values = dec_values./sum(dec_values);
                    %SaArr(kk) = sum(dec_values(negidx));
                    
                    SaArr(kk) = length(negidx)./length(TotalIdxSet);
                end
                
                sdist= sqrt(((ctop + cbottom)/2 - (1+m)/2).^2 + ((cleft + cright)/2 - (1+n)/2).^2); 
                map(ctop:cbottom,cleft:cright) = map(ctop:cbottom,cleft:cright)+ mean(SaArr)*exp(-sdist/min(m, n)); %.*(1+min(AngleArr));
                
            otherwise
                disp('Please choose the right method.')
        end     

        progress = progress+1./total; 
        if (progress > 0.025)
            progress = progress - 0.025;
            fprintf('.')
        end
        
    end
end
toc

%%
%normalize and blur 
g = fspecial('gaussian',[8 8],8);
map = imfilter(map,g).^2;
map = (map-min(min(map)))./(max(max(map))-min(min(map)));



