function master_map = LocalSvmSaliency(im, m, n, option)
%% SVM saliency 
master_map = zeros(m,n);
size_v{1} = [0.15]; size_v{2} = [0.25];
size_v{3} = [0.35]; size_v{4} = [0.45];
size_v{5} = [0.5];


size_h{1} = [0.15]; size_h{2} = [0.25];
size_h{3} = [0.35]; size_h{4} = [0.45];
size_h{5} = [0.5];

patch_size = [8 8 8 8 8];  alpha = [5 7 7 7 7];
for i=1:3
    if m > n
        img = imresize(im, size_v{i}, 'bicubic');
        fprintf('%dth scale \n at resolution [%d, %d]',i, size(img,1), size(img,2));
    else
        img = imresize(im, size_h{i}, 'bicubic');
        fprintf('%dth scale at resolution [%d, %d] \n ',i, size(img,1), size(img,2));
    end
    img = double(img)./255;
    
    FeaMat = img;
    switch option 
        case 'WeightedLSSVM'
              map{i} = sparse_saliency_v3(FeaMat, patch_size(i), alpha(i), 0.01, [], 'svmc');
        case 'WeightedStandardSVM'      
              map{i} = sparse_saliency_v3(FeaMat, patch_size(i), alpha(i), 0.01, [], 'svmc');
        otherwise
              map{i} = sparse_saliency_v3(FeaMat, patch_size(i), alpha(i), 0.01, [], 'svmc');
     end
     master_map = master_map + imresize(abs(map{i}), [m n]);
    i
end
master_map = (master_map - min(master_map(:))) / (max(master_map(:)) - min(master_map(:)) + eps);
figure, imagesc(master_map); colormap('gray'); title('Svm saliency map');
