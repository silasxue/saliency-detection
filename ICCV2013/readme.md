# Matlab/C++ code for the paper "Contextual Hypergraph Modeling for Salient Object Detection"
  by X. Li , Y. Li, C. Shen, A. Dick, A. van den Hengel. ICCV 2013.

The paper can be downloaded at [http://arxiv.org/abs/1310.5767](http://arxiv.org/abs/1310.5767)



## This code is using the GNU license. Use at your own risk.


- Mex all c++ files in the following folers (For windows):

.. (a) run "Agglo_MS_compile.m" in the folder "./ClusteringUtil";

.. (b) run "compile_edison_wrapper.m" in the folder "./edison_matlab_interface";

.. (c) run "libsvm_compile.m" in the folder "./libsvm";

.. (d) compile ‘cd GraphSegment; mex mexSegment.cpp’

For Linux and OSX, you may need to modify the compiling commands.
We have included the compiled mex files for Win32 and OSX64. Tested on Windows 7 and OSX10.9.


- Perform superpixel segmentation for the image folder "./Images"

.. (a) run "ExtractMeanShiftSegmentationMask.m" to obtain mean shift based superpixels;

.. (b) run "ExtractSlicSegmentationMask.m" to obtain graph clustering based superpixels.

- Run “demo.m" to generate the saliency maps for the images from "./Images".


If you use this code in your research, please cite our paper:

```
@inproceedings{ICCV13Li,
   author    = "X. Li and  Y. Li and  C. Shen and  A. Dick and  A. {van den Hengel}",
   title     = "Contextual hypergraph modeling for salient object detection",
   booktitle = "IEEE Conference on Computer Vision (ICCV'13)",
   address   = "Sydney, Australia",
   year      = "2013",
 }
```
